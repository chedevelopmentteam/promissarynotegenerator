﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace PromissoryNoteGenerator.Classes
{
    internal class GenerateFile
    {
        private readonly bool _isTest;

        public bool IsTest
        {
            get { return _isTest; }
        }

        public GenerateFile(string fileName, bool isTest)
        {
            _isTest = isTest;
            var layout = new AwardFileLayout();
            var output = new StringBuilder();
            var badRows = new StringBuilder();
            using (var reader = new CsvFileReader(fileName))
            {
                
                var row = new CsvRow();
                var header = true;
                while (reader.ReadRow(row))
                {
                    //Do not extract first row as it is the Header and is not required for the export
                    if (header)
                    {
                        header = false;
                    }
                    else
                    {
                        var outputRow = String.Empty;
                        //Column counter, used value but blank fields keep throwing logic for a loop.
                        var colNo = -1;
                        
                        foreach (var colValue in row)
                        {
                            var col = colValue;
                            colNo++;
                            //Get Base Rules 
                            var formatRule = layout.LayoutElements[colNo].Format;
                            var baseLength = layout.LayoutElements[colNo].Length;
                            var required = layout.LayoutElements[colNo].Required;
                            var typeRule = layout.LayoutElements[colNo].Type;
                            var field = layout.LayoutElements[colNo].Field;
                            //Check Required Status 
                            if (colValue.Length == 0 && required)
                            {
                                //If failed then add to error file - BadRows
                                badRows.AppendLine("Row Missing:" + layout.LayoutElements[row.IndexOf(colValue)].Field);
                                badRows.AppendLine(row.ToString());
                                break;

                            }
                            //Apply Type Requirement
                            switch (typeRule)
                            {
                                case "currency":
                                    if(colValue.Length > 3)
                                        col = colValue.Substring(0, colValue.Length-2) + "." + colValue.Substring(colValue.Length-2, 2);
                                    break;
								case "date":
		                            if (colValue.Length == 7)
		                            {
			                            col = "0" + colValue;
		                            }

									break;
	                            default:
                                    break;
                            }

                            if (field == "Loan Period")
                            {
                                var size = colValue.Length;
                            }
                            //Apply any Rule Requirement
                            switch (formatRule)
                            {
                                case "right justified":
                                    outputRow += FormatString(col, baseLength, "right");
                                    break;
                                case "no punctuation":
                                    outputRow += FormatString(ClearPunctuation(col), baseLength, "");
                                    break;
                                 
                                default:
                                    outputRow += FormatString(col, baseLength, "left");
                                    break;
                            }
                        }

                        output.AppendLine(outputRow);
                    
                    }

                   

                }
            }
            if (output.Length > 0)
            {
                WriteFile(output, FileNameGenerator("C78", IsTest));
            }
            if (badRows.Length > 0)
            {
                WriteFile(badRows, "RowsMissingRequiredFields");
            }
        }

        private string FileNameGenerator(string tag, bool isTest)
        {
            string test = isTest ? "Test" : String.Empty;
            string mn = DateTime.Now.Month < 10 ? $"0{DateTime.Now.Month}" : DateTime.Now.Month.ToString();
            string dy = DateTime.Now.Day < 10 ? $"0{DateTime.Now.Day}" : DateTime.Now.Day.ToString();
            return $"{test}bldprom-{tag}-{DateTime.Now.Year}{mn}{dy}.txt";
        }

        private static string FormatString(string s, int baseLength, string justify)
        {
            {
                

                if (s.Length > baseLength)
                {
                    s = s.Substring(0, baseLength);
                }
                //Add spaces to front of value
                while (s.Length < baseLength)
                {
                    if (justify == "right")
                    {
                        s = " " + s;
                    }
                    else
                    {
                       //Add spaces to rear of value
                       s = s + " ";
                       
                    }
                }
            }
            return s;
        }

        private static string ClearPunctuation(string s)
        {
            //Clear out any punctuation
            if (s.Contains("-"))
                s = s.Replace("-", string.Empty);
           if (s.Contains("("))
                s = s.Replace("(", string.Empty);
            if (s.Contains(")"))
                s = s.Replace(")", string.Empty);

            return s;
        }

        private static void WriteFile(StringBuilder sb, string fileName)
        {
            
            using (SaveFileDialog dialog = new SaveFileDialog())
            {
                dialog.FileName = fileName;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    TextWriter writer = new StreamWriter(dialog.FileName);
                    writer.Write(sb.ToString());
                    writer.Flush();
                    writer.Close();
                }
            }
        }

        


    }
}
