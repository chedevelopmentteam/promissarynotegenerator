﻿namespace PromissoryNoteGenerator
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblSourceFile = new System.Windows.Forms.Label();
            this.btnGetSource = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnProcess = new System.Windows.Forms.Button();
            this.chkTestFile = new System.Windows.Forms.CheckBox();
            this.lblDone = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Source File (csv only)";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // lblSourceFile
            // 
            this.lblSourceFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSourceFile.Location = new System.Drawing.Point(15, 30);
            this.lblSourceFile.Name = "lblSourceFile";
            this.lblSourceFile.Size = new System.Drawing.Size(365, 28);
            this.lblSourceFile.TabIndex = 1;
            this.lblSourceFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnGetSource
            // 
            this.btnGetSource.BackColor = System.Drawing.Color.BurlyWood;
            this.btnGetSource.Location = new System.Drawing.Point(12, 61);
            this.btnGetSource.Name = "btnGetSource";
            this.btnGetSource.Size = new System.Drawing.Size(118, 25);
            this.btnGetSource.TabIndex = 2;
            this.btnGetSource.Text = "Get Source File";
            this.btnGetSource.UseVisualStyleBackColor = false;
            this.btnGetSource.Click += new System.EventHandler(this.BtnGetSource_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnProcess
            // 
            this.btnProcess.BackColor = System.Drawing.Color.CadetBlue;
            this.btnProcess.Enabled = false;
            this.btnProcess.Location = new System.Drawing.Point(151, 63);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(128, 23);
            this.btnProcess.TabIndex = 3;
            this.btnProcess.Text = "Process File";
            this.btnProcess.UseVisualStyleBackColor = false;
            this.btnProcess.Click += new System.EventHandler(this.BtnProcess_Click);
            // 
            // chkTestFile
            // 
            this.chkTestFile.AutoSize = true;
            this.chkTestFile.Location = new System.Drawing.Point(300, 69);
            this.chkTestFile.Name = "chkTestFile";
            this.chkTestFile.Size = new System.Drawing.Size(66, 17);
            this.chkTestFile.TabIndex = 4;
            this.chkTestFile.Text = "Test File";
            this.chkTestFile.UseVisualStyleBackColor = true;
            // 
            // lblDone
            // 
            this.lblDone.BackColor = System.Drawing.Color.SeaGreen;
            this.lblDone.Location = new System.Drawing.Point(12, 107);
            this.lblDone.Name = "lblDone";
            this.lblDone.Size = new System.Drawing.Size(354, 24);
            this.lblDone.TabIndex = 5;
            this.lblDone.Text = "Done with Processing";
            this.lblDone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDone.Visible = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 282);
            this.Controls.Add(this.lblDone);
            this.Controls.Add(this.chkTestFile);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.btnGetSource);
            this.Controls.Add(this.lblSourceFile);
            this.Controls.Add(this.label1);
            this.Name = "frmMain";
            this.Text = "frmMain";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSourceFile;
        private System.Windows.Forms.Button btnGetSource;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.CheckBox chkTestFile;
        private System.Windows.Forms.Label lblDone;
    }
}