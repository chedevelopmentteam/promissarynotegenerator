﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PromissoryNoteGenerator.Classes;

namespace PromissoryNoteGenerator
{
    public partial class FrmMain : Form
    {
        private string _path;
        public FrmMain()
        {
            InitializeComponent();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void BtnGetSource_Click(object sender, EventArgs e)
        {
            lblDone.Visible = false;
	        OpenFileDialog ofd = new OpenFileDialog {Filter = @"csv file (*.csv) | *.csv"};
	        if (ofd.ShowDialog() == DialogResult.OK)
            {

                lblSourceFile.Text = $@"{Path.GetDirectoryName(ofd.FileName)}\{ofd.FileName}";
                // ReSharper disable once AssignNullToNotNullAttribute
                _path = Path.Combine(Path.GetDirectoryName(ofd.FileName), ofd.FileName);
                btnProcess.Enabled = true;
                btnGetSource.Enabled = false;
            }
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            btnProcess.Enabled = false;
	        // ReSharper disable once ObjectCreationAsStatement
            new GenerateFile(_path,  chkTestFile.Checked);
            lblDone.Visible = true;
            btnGetSource.Enabled = true;
        }
    }
}
