﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromissoryNoteGenerator.Classes
{
    class AwardFileLayout
    {
        public  List<AwardFileElement> LayoutElements { get; private set; }

        public AwardFileLayout()
        {
            LayoutElements = new List<AwardFileElement>
            {
                new AwardFileElement("SSN", 9, "numeric", true, "none"),
                new AwardFileElement("SID#", 9, "numeric", false, "none"),
                new AwardFileElement("Loan Fund",8,"string",true,"none"),
                new AwardFileElement("Campus",2,"string",false,"none"),
                new AwardFileElement("First Name",20,"string",true,"none"),
                new AwardFileElement("Middle Initial",1,"string",false,"none"),
                new AwardFileElement("Last Name",20,"string",true,"none"),
                new AwardFileElement("Address 1",40,"string",false,"none"),
                new AwardFileElement("Address 2",40,"string",false,"none"),
                new AwardFileElement("City",25,"string",false,"none"),
                new AwardFileElement("State",2,"string",false,"none"),
                new AwardFileElement("ZIP Code",9,"string",false,"no punctuation"),
                new AwardFileElement("Date of Birth",8,"date",true,"none"),
                new AwardFileElement("Phone",10,"string",false,"no punctuation"),
                new AwardFileElement("Loan Amount",10,"currency",false,"no punctuation"),
                new AwardFileElement("Loan Period",20,"numeric",false,"none"),
                new AwardFileElement("Drivers Lic#",20,"string",false,"none"),
                new AwardFileElement("Drivers Lic State",2,"string",false,"none"),
                new AwardFileElement("Email",64,"string",true,"none"),
                new AwardFileElement("Application Fee",10,"numeric",false,"right justified"),
                new AwardFileElement("Orgination Fee",10,"numeric",false,"right justified"),
                new AwardFileElement("Repayment Fee",10,"numeric",false,"right justified"),
                new AwardFileElement("Cost of Attendence",10,"numeric",false,"right justified"),
                new AwardFileElement("Est. FinAid",10,"numeric",false,"right justified"),
                new AwardFileElement("Expected Grad Date",8,"date",false,"none")
            };

        }
    }

    class AwardFileElement
    {
        public string Field { get; private set; }
        public int Length { get; private set; }
        public string Type { get; private set; }
        public  bool Required { get; private set; }
        public string Format { get; private set; }

        public AwardFileElement(string field, int length, string type, bool required, string format)
        {
            Field = field;
            Length = length;
            Type = type;
            Required = required;
            Format = format;
        }

    }
}
